<?php

require('animal.php');
require('frog.php');
require('ape.php');

$object = new Animal("shaun");

echo "Nama Hewan : $object->name <br>";
echo "Jumlah Kaki : $object->kaki <br>";
echo "Berdarah Dingin : $object->coldBlood <br><br>";

$kodok = new Frog("buduk");

echo "Nama Hewan : $kodok->name <br>";
echo "Jumlah Kaki : $kodok->kaki <br>";
echo "Berdarah Dingin : $kodok->coldBlood <br>";
$kodok->jump();

$sungkong= new Ape("kera sakti");

echo "Nama Hewan : $sungkong->name <br>";
echo "Jumlah Kaki : $sungkong->kaki <br>";
echo "Berdarah Dingin : $sungkong->coldBlood <br>";
$sungkong->yell();

// index.php
// $sungokong = new Ape("kera sakti");
// $sungokong->yell() // "Auooo"

// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"