@extends('layouts.master')

@section('judul')
    Edit Pemeran
@endsection

@section('content')
<form method="post" action="/cast/{{$castData->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Pemeran</label>
      <input type="text" name="name" value="{{$castData->nama}}" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Pemeran">
    </div>
    @error('name')
        <div class="aler alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Usia</label>
      <input type="number" name="umur" value="{{$castData->umur}}" class="form-control @error('umur') is-invalid @enderror" placeholder="Masukkan Usia">
    </div>
    @error('umur')
        <div class="aler alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" id="" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror">{{$castData->bio}}</textarea>
    </div>
    @error('bio')
        <div class="aler alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection