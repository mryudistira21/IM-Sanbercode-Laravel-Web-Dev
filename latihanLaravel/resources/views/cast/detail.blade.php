@extends('layouts.master')

@section('judul')
    Detail Pemeran
@endsection

@section('content')
    <h1 class="text-primary">{{$castData->nama}}</h1>
    <p>Umur : {{$castData->umur}}</p>
    <p>Biodata : {{$castData->bio}}</p> 
@endsection