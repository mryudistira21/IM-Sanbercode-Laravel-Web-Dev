@extends('layouts.master')

@section('judul')
    Home
@endsection

@section('content')
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}</h1>
    <h2>Terimakasih telah bergabung di Sanbercode. Social Media kita Bersama!</h2>
@endsection