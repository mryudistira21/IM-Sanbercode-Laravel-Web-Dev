@extends('layouts.master')

@section('judul')
Sign Up Form
@endsection

@section('content')
    <form action="/home" method="post">
        @csrf
        <label>Full Name</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender</label><br>
        <input type="radio" id="male" name="gender" value="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">Others</label><br>
        <label>Nationality</label><br>
        <select name="nationality">
            <option value="ina">Indonesia</option>
            <option value="mal">Malaysia</option>
            <option value="sin">Singapore</option>
            <option value="tha">Thailand</option>
        </select><br><br>
        <label>Languange Spoken</label><br>
        <input type="checkbox" name="languange">Bahasa Indonesia<br>
        <input type="checkbox" name="languange">English<br>
        <input type="checkbox" name="languange">Arabic<br>
        <input type="checkbox" name="languange">Japanese<br><br>
        <label>Bio</label><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection