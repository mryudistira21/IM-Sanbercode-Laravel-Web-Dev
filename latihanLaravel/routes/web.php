<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\BioController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'utama']);
Route::get('/daftar', [BioController::class, 'daftar']);
Route::post('/home', [BioController::class, 'home']);

Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});

//CRUD Cast : Add and Show Data
Route::get('/cast', [CastController::class,'index']);
Route::post('/cast', [CastController::class,'store']); //Add data
//CRUD Cast : Create Data
Route::get('/cast/create', [CastController::class,'create']);

//CRUD Cast : View, Edit, Update, and Delete by CastID
Route::get('/cast/{cast_id}', [CastController::class,'show']);
Route::put('/cast/{cast_id}', [CastController::class,'update']);
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
