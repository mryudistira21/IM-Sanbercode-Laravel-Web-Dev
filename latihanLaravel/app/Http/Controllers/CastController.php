<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast=DB::table('cast')->get();

        return view('cast.tampil', ['cast'=>$cast]);
    }   

    public function store(Request $request){
        //validate
        $request->validate([
            'name'=>'required|min:5',
            'umur'=>'required',
            'bio'=>'required',
        ]);

        //insert data to database
        DB::table('cast')->insert([
            'nama'=>$request->input('name'),
            'umur'=>$request->input('umur'),
            'bio'=>$request->input('bio')
        ]);

        //Direct to /cast page
        return redirect('/cast');
    }

    public function create(){
        return view('cast.tambah');
    }

    public function show($cast_id){
        $castData=DB::table('cast')->find($cast_id);

        return view('cast.detail', ['castData'=>$castData]);
    }

    public function edit($cast_id){
        $castData=DB::table('cast')->find($cast_id);

        return view('cast.edit', ['castData'=>$castData]);
    }

    public function update($cast_id, Request $request){
        //validate
        $request->validate([
            'name'=>'required|min:5',
            'umur'=>'required',
            'bio'=>'required',
        ]);

        //Update Data
        DB::table('cast')
            ->where('id', $cast_id)
            ->update(
                [
                'nama'=>$request->input('name'),
                'umur'=>$request->input('umur'),
                'bio'=>$request->input('bio')
                ]
            );
        //Redirect Halaman
        return redirect('/cast');
    }

    public function destroy($cast_id){
        DB::table('cast')->where('id','=',$cast_id)->delete();

        return redirect('/cast');
    }
}